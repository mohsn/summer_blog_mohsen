﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace mohsen.Models
{
    public class Gallery
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter a Title.")]
        public string Title { get; set; }

        public string Description { get; set; }


        public string Imgurl { get; set; }
        
     
         
    }
}
